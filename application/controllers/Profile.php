<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('profile/show');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }

    public function show()
    {
        if ($this->session->userdata('email')) {
            $data['user'] = $this->db->get_where('ta_pbw2_users', ['email' => $this->session->userdata('email')])->row_array();
            //echo "Nama : " . $data['user']['name'];
            $data['title'] = 'Aplikasi Agenda - Profil Saya';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navigation', $data);
            $this->load->view('profile/show', $data);
            $this->load->view('templates/footer');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }

    public function edit()
    {
        if ($this->session->userdata('email')) {
            $data['user'] = $this->db->get_where('ta_pbw2_users', ['email' => $this->session->userdata('email')])->row_array();
            $data['title'] = 'Aplikasi Agenda - Edit Profil';
            $this->form_validation->set_rules('name', 'Nama Lengkap', 'required|trim');

            if ($this->form_validation->run() ==  false) {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/navigation', $data);
                $this->load->view('profile/edit', $data);
                $this->load->view('templates/footer');
            } else {
                $name = $this->input->post('name');
                $id = $this->input->post('id');
                $bio = $this->input->post('bio');
                $data = array(
                    "name" => $name,
                    "bio"  => $bio,
                );
                $this->db->set($data);
                $this->db->where('id', $id);
                $this->db->update('ta_pbw2_users');

                // cek gambar
                $upload_image = $_FILES['image'];

                if ($upload_image) {
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['max_size'] = '2048';
                    $config['upload_path'] = './assets/profile-img/';
                    $config['max_height'] = '1600';
                    $config['max_width'] = '1600';

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('image')) {
                        $old_image = $data['user']['image'];
                        if ($old_image != 'default.jpg') {
                            unlink('FCPATH' . '/assets/profile-img/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                        $this->db->set('image', $new_image);
                        $this->db->where('id', $id);
                        $this->db->update('ta_pbw2_users');
                    }
                }
                if ($new_image || $this->db->affected_rows()) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success" align="center" role="alert">Profil anda sudah diperbarui</div>');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">' . $this->upload->display_errors() . '</div>');
                }
                redirect('profile/show');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }

    public function passwd()
    {
        if ($this->session->userdata('email')) {
            $data['user'] = $this->db->get_where('ta_pbw2_users', ['email' => $this->session->userdata('email')])->row_array();
            //echo "Nama : " . $data['user']['name'];
            $data['title'] = 'Aplikasi Agenda - Edit Profil';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navigation', $data);
            $this->load->view('profile/passwd', $data);
            $this->load->view('templates/footer');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }
}
