<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('email')) {
            $data['user'] = $this->db->get_where('ta_pbw2_users', ['email' => $this->session->userdata('email')])->row_array();
            $data['title'] = 'Aplikasi Agenda - Agenda';
            $data_agenda['events'] = $this->db->get_where('ta_pbw2_events', ['email' => $this->session->userdata('email')])->result_array();
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navigation', $data);
            $this->load->view('agenda', $data_agenda);
            $this->load->view('templates/footer');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }

    public function add()
    {
        $this->db->insert('ta_pbw2_events', $_POST);
        $this->session->set_flashdata('message', '<div align="center" class="alert alert-success alert-dismissible fade show" role="alert">
        Agenda anda telah ditambahkan
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
        redirect('agenda');
    }

    public function edit()
    {
        $this->db->where('id', $_POST['id']);
        $this->db->update('ta_pbw2_events', $_POST);
        $this->session->set_flashdata('message', '<div align="center" class="alert alert-success alert-dismissible fade show" role="alert">
        Agenda anda telah diperbarui!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
        redirect('agenda');
    }

    public function delete($id)
    {
        $this->db->delete('ta_pbw2_events', ['id' => $id]);
        $this->session->set_flashdata('message', '<div align="center" class="alert alert-success alert-dismissible fade show" role="alert">
        Agenda anda telah dihapus!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
        redirect('agenda');
    }
}
