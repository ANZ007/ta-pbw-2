<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('profile/show');
        }
        redirect('auth/login');
    }

    public function login()
    {
        if ($this->session->userdata('email')) {
            redirect('profile/show');
        }

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Aplikasi Agenda - Login';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else
            $this->_login();
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('ta_pbw2_users', ['email' => $email])->row_array();

        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'email' => $user['email'],
                ];
                $this->session->set_userdata($data);
                redirect('profile/show');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Password salah !</div>');
                redirect('auth/login');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email belum terdaftar!</div>');
            redirect('auth/login');
        }
    }

    public function register()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('name', 'Nama', 'required|trim');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[ta_pbw2_users.email]');
        $this->form_validation->set_rules('password', 'Kata sandi', 'required|trim|min_length[4]|matches[repassword]');
        $this->form_validation->set_rules('repassword', 'Ulang kata sandi', 'required|trim|matches[password]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Aplikasi Agenda - Registrasi';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/register');
            $this->load->view('templates/auth_footer');
        } else {
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'dob' => htmlspecialchars($this->input->post('dob', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'image' => 'default_profile.jpg',
                'date_created' => time()
            ];

            $this->db->insert('ta_pbw2_users', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Selamat! akun anda telah teregistrasi. Silahkan login. </div>');
            redirect('auth/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Anda sudah logout </div>');
        redirect('auth/login');
    }
}
