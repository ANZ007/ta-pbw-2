<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calendar extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('email')) {
            $data['user'] = $this->db->get_where('ta_pbw2_users', ['email' => $this->session->userdata('email')])->row_array();
            $data['title'] = 'Aplikasi Agenda - Kalender';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navigation', $data);
            $this->load->view('calendar');
            $this->load->view('templates/footer');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda belum login !</div>');
            redirect('auth/login');
        }
    }

    public function events_load()
    {
        $email = $this->session->userdata('email');
        $this->db->order_by('id');
        $event_data = $this->db->get_where('ta_pbw2_events', ['email' => $email]);
        foreach ($event_data->result_array() as $row) {
            $data[] = array(
                'id' => $row['id'],
                'title' => $row['title'],
                'start' => $row['start'],
                'end' => $row['end']
            );
        }
        echo json_encode($data);
    }

    public function events_add()
    {
        $email = $this->session->userdata('email');
        if ($this->input->post('title')) {
            $data = array(
                'email' => $email,
                'title'  => $this->input->post('title'),
                'start' => $this->input->post('start'),
                'end' => $this->input->post('end')
            );
            $this->db->insert('ta_pbw2_events', $data);
        }
    }

    public function events_update()
    {
        if ($this->input->post('id')) {
            $data = array(
                'title'   => $this->input->post('title'),
                'start' => $this->input->post('start'),
                'end'  => $this->input->post('end')
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('ta_pbw2_events', $data);
        }
    }

    public function events_delete()
    {
        if ($this->input->post('id')) {
            $this->db->where('id', $this->input->post('id'));
            $this->db->delete('ta_pbw2_events');
        }
    }
}
