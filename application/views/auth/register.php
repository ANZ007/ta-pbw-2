<body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <a href="<?= base_url(); ?>">Aplikasi Agenda</a>
        </div>

        <div class="card">
            <div class="card-body register-card-body">
                <p class="login-box-msg">Registrasi akun baru</p>

                <form action="<?= base_url('auth/register'); ?>" method="post">
                    <div class="input-group">
                        <input type="text" name="name" class="form-control" value="<?= set_value('name'); ?>" placeholder="Nama lengkap">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <?= form_error('name', '<small class="text-danger">', '</small>'); ?>
                    <div class="input-group mt-3">
                        <input type="text" name="dob" class="form-control" onfocus="(this.type='date')" value="<?= set_value('dob'); ?>" placeholder="Tanggal Lahir">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-calendar"></span>
                            </div>
                        </div>
                    </div>
                    <?= form_error('dob', '<small class="text-danger">', '</small>'); ?>
                    <div class="input-group mt-3">
                        <input type="text" name="email" class="form-control" value="<?= set_value('email'); ?>" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                    <div class="input-group mt-3">
                        <input type="password" name="password" class="form-control" placeholder="Kata sandi">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <?= form_error('password', '<small class="text-danger">', '</small>'); ?>
                    <div class="input-group mt-3">
                        <input type="password" name="repassword" class="form-control" placeholder="Ketik ulang kata sandi">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <hr>
                <a href="<?= base_url('auth/login') ?>" class="text-center">Saya sudah punya akun</a>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->