<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        Powered by <b><a href="http://adminlte.io">AdminLTE </a>Version</b> 3.0.4
    </div>
    <strong>Copyright &copy; <?= date('Y'); ?> Aplikasi Agenda.</strong> All rights reserved.
</footer>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih "Logout" dibawah jika anda ingin mengakhiri sesi.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <a class="btn btn-primary" href="<?= base_url('auth/logout') ?>">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url('assets'); ?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="<?= base_url('assets'); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- fullCalendar script -->
<script src="<?= base_url('assets'); ?>/plugins/moment/moment.min.js"></script>
<script src="<?= base_url('assets'); ?>/dist/js/fullcalendar.min.js"></script>
<script src="<?= base_url('assets'); ?>/dist/js/locale-all-fullcalendar.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets'); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- pace-progress -->
<script src="<?= base_url('assets'); ?>/plugins/pace-progress/pace.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets'); ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets'); ?>/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets'); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets'); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets'); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- Page specific script -->
<!-- Parsing data to edit modal in agenda -->
<script>
    $('#modalEdit').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('id');
        $(e.currentTarget).find('input[name="id"]').val(id);
    });
    $('#modalDelete').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('id');
        $(e.currentTarget).find('input[name="id"]').val(id);
    });
</script>
<!-- show image name in edit profile -->
<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
</script>
<!-- agenda -->
<script>
    $(function() {
        $('#agenda').DataTable({
            "lengthMenu": [
                [5, 10, 25, 50, 100, -1],
                [5, 10, 25, 50, 100, "Semua"]
            ],
            "language": {
                "processing": "Sedang memproses...",
                "lengthMenu": "Menampilkan _MENU_ daftar per halaman",
                "zeroRecords": "Tidak ditemukan agenda",
                "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 daftar",
                "infoFiltered": "(difilter dari _MAX_ total daftar)",
                "search": "Cari : ",
                "paginate": {
                    "first": "Pertama",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya",
                    "last": "Terakhir"
                }
            },
            "paging": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
<!--Kalender-->
<script>
    $(document).ready(function() {
        function fmt(date) {
            return date.format("YYYY-MM-DD HH:mm");

        }

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var calendar = $('#calendar').fullCalendar({
            locale: 'id',
            editable: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },

            events: "<?= base_url('calendar/events_load') ?>",

            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Judul Agenda:');
                if (title) {
                    var start = fmt(start);
                    var end = fmt(end);
                    $.ajax({
                        url: '<?= base_url('calendar/events_add') ?>',
                        data: 'title=' + title + '&start=' + start + '&end=' + end,
                        type: "POST",
                        success: function(json) {
                            calendar.fullCalendar('refetchEvents');
                            alert('Sukses ditambahkan');
                        }
                    });
                    calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true
                    );
                }
                calendar.fullCalendar('unselect');
            },

            editable: true,
            eventDrop: function(event, delta) {
                var start = fmt(event.start);
                var end = fmt(event.end);
                $.ajax({
                    url: '<?= base_url('calendar/events_update') ?>',
                    data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                    type: "POST",
                    success: function(json) {
                        calendar.fullCalendar('refetchEvents');
                        alert("Sukses Terupdate");
                    }
                });
            },
            eventClick: function(event) {
                var decision = confirm("Apakah anda ingin menghapus agenda ini ?");
                if (decision) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('calendar/events_delete') ?>",
                        data: "&id=" + event.id,
                        success: function(json) {
                            calendar.fullCalendar('refetchEvents');
                            alert("Sukses Terhapus");
                        }
                    });


                }

            },
            eventResize: function(event) {
                var start = fmt(event.start);
                var end = fmt(event.end);
                $.ajax({
                    url: '<?= base_url('calendar/events_update') ?>',
                    data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                    type: "POST",
                    success: function(json) {
                        alert("Sukses Terupdate");
                    }
                });

            }

        });

    });
</script>
</body>

</html>
