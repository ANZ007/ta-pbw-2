<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Agenda Saya</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item active">Agenda Saya</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('message'); ?>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="agenda" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama agenda</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($events as $event) : ?>
                                    <tr>
                                        <td><?php echo $no;
                                            $no++; ?></td>
                                        <td><?= $event['title'] ?></td>
                                        <td><?php setlocale(LC_ALL, 'id_ID.UTF-8');
                                            echo strftime('%A, %e %B %Y', strtotime($event['start'])); ?></td>
                                        <td><?php setlocale(LC_ALL, 'id_ID.UTF-8');
                                            echo strftime('%A, %e %B %Y', strtotime($event['end'])); ?></td>
                                        <td>
                                            <a class="btn btn-warning" href="<?= base_url('agenda/edit/') . $event['id']; ?>" data-toggle="modal" data-target="#modalEdit" data-id="<?= $event['id']; ?>" role="button">Edit</a>
                                            <a class="btn btn-danger" href="<?= base_url('agenda/delete/') . $event['id']; ?>" role="button">Hapus</a>
                                        </td>
                                    <?php endforeach ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama agenda</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modalAdd" role="button">Tambah</a>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal -->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitleAdd">Tambah Agenda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('agenda/add') ?>" method="post">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="email" id="email" value="<?= $this->session->userdata('email'); ?>">
                        <div class=" form-group">
                            <label for="title">Nama agenda</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Nama agenda" required>
                        </div>
                        <div class="form-group">
                            <label for="start">Tanggal Mulai</label>
                            <input type="date" class="form-control" id="start" name="start" required>
                        </div>
                        <div class="form-group">
                            <label for="end">Tanggal Selesai</label>
                            <input type="date" class="form-control" id="end" name="end" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitleAdd">Edit Agenda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('agenda/edit') ?>" method="post">
                        <input type="hidden" class="form-control" name="id" id="id">
                        <input type="hidden" name="email" id="email" value="<?= $this->session->userdata('email'); ?>">
                        <div class=" form-group">
                            <label for="title">Nama agenda</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Nama agenda" required>
                        </div>
                        <div class="form-group">
                            <label for="start">Tanggal Mulai</label>
                            <input type="date" class="form-control" id="start" name="start" required>
                        </div>
                        <div class="form-group">
                            <label for="end">Tanggal Selesai</label>
                            <input type="date" class="form-control" id="end" name="end" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-warning">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
